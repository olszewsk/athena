/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// Generic overlaying code for Muon Digits 
// Factored out from InDetOverlay.
//
// Andrei Gaponenko <agaponenko@lbl.gov>, 2006-2008

// Ketevi A. Assamagan <ketevi@bnl.gov>, March 2008

// Piyali Banerjee <Piyali.Banerjee@cern.ch>, March 2011

#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGateSvc.h"
#include <memory>

//================================================================

template<class TypeToBeCopied, class Datum> 
TypeToBeCopied* MuonOverlayBase::copyMuonDigitContainer(const TypeToBeCopied* oldObject)
{

  typedef typename TypeToBeCopied::base_value_type Collection;
  const std::string templateClassName = typeid(TypeToBeCopied).name();

  if (oldObject == 0) {
    ATH_MSG_WARNING("copyMuonDigitContainer<"<<templateClassName<<">(): oldObject is a NULL pointer");
    return 0;
  }

  TypeToBeCopied *newObject = 0;

  newObject = new TypeToBeCopied (oldObject->size());
  typename TypeToBeCopied::const_iterator iFirst = oldObject->begin();
  typename TypeToBeCopied::const_iterator iLast = oldObject->end();
  for (; iFirst != iLast; ++iFirst ) {
     Collection *element = new Collection ((*iFirst)->identify(), (*iFirst)->identifierHash());
     typename TypeToBeCopied::base_value_type::const_iterator firstData = (*iFirst)->begin();
     typename TypeToBeCopied::base_value_type::const_iterator lastData = (*iFirst)->end();
     for (; firstData != lastData; ++firstData) {
        Datum * newData = new Datum (*(dynamic_cast<const Datum*>(*firstData)));
        element->push_back(newData);
     }
     if ( newObject->addCollection ( element, (*iFirst)->identifierHash() ).isFailure() ) {
        ATH_MSG_WARNING("copyMuonDigitCongtainer<"<<templateClassName<<">(): problem adding collection with "<<"hash="<<(*iFirst)->identifierHash() );
     }
  }

  return newObject;

}
